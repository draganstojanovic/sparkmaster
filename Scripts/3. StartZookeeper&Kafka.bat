@echo off

cd C:\kafka_2.11-1.0.0\bin\windows
FOR /D /R %%X IN (kafka_*) DO RD /S /Q "%%X"
cd C:\Users\marin\Desktop\SPARK_scripts
FOR /D /R %%X IN (kafka_*) DO RD /S /Q "%%X"
TIMEOUT /T 2
start C:\zookeeper-3.4.10\bin\zkServer.cmd
TIMEOUT /T 5
start C:\kafka_2.11-1.0.0\bin\windows\kafka-server-start.bat C:\kafka_2.11-1.0.0\config\server.properties
pause