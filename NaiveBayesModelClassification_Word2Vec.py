# -*- coding: utf-8 -*-
"""
Created on Tue Mar 19 23:07:02 2019

@author: marin
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Mar 20 22:20:11 2018

@author: marin
"""
from pyspark import SparkContext
from pyspark.sql import SparkSession
from pyspark.ml.feature import HashingTF, IDF, Tokenizer
from pyspark.ml.feature import CountVectorizer
from pyspark.ml.feature import StopWordsRemover
import csv
import re
import string
from pyspark.sql.types import FloatType
from pyspark.mllib.regression import LabeledPoint
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.stem import LancasterStemmer
from pyspark.mllib.classification import NaiveBayes 
from pyspark.mllib.evaluation import MulticlassMetrics
from pyspark.ml.feature import IDFModel
from pyspark.ml.feature import CountVectorizerModel

stemmer =  LancasterStemmer()
lemmer = WordNetLemmatizer()

from nltk.tokenize import wordpunct_tokenize, RegexpTokenizer

alpha_tokenizer = RegexpTokenizer('[A-Za-z]\w+')

def lemmatizeWords(tokens):
    for index, word in enumerate(tokens): 
        lower = word.lower();
        tokens[index] = lemmer.lemmatize(lower)
    return tokens

def cleanText(text):
    cleaned_text = re.sub(r'http[s]?:\/\/.*[\W]*', '', text, flags=re.MULTILINE)# remove urls
    cleaned_text = re.sub(r'@[\w]*', '', cleaned_text, flags=re.MULTILINE) # remove the @twitter mentions 
    cleaned_text = re.sub(r'RT.*','', cleaned_text, flags=re.MULTILINE)  # delete the retweets
    cleaned_text = cleaned_text.replace('\n', ' ') #replace enters
    cleaned_text = cleaned_text.translate(None, string.punctuation)
    cleaned_text = ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)"," ",cleaned_text).split())
    return cleaned_text
	
def getNaiveBayesModelData(sparkContext, path):
    training_initial = sparkContext.textFile(path)
    training_partitions = training_initial.mapPartitions(lambda x: csv.reader(x, delimiter='`', quotechar='|'))
    training = training_partitions.map(lambda row: (row[1],cleanText(row[0]))).cache()
    return training

def createDataFrameData(sparkSession, trainingData):
    dataFrame = sparkSession.createDataFrame(trainingData)
    dataFrame.show()
    return dataFrame
	
def createAndMapDataToDataFrame(wordsDataRemoved):
    wordsDataRemoved = wordsDataRemoved.rdd.map(lambda el: (el["_1"],el["_2"],el["words"], lemmatizeWords(el["wordsremoved"])))
    wordsDataRemoved = spark.createDataFrame(wordsDataRemoved)
    wordsDataRemoved.show()
    return wordsDataRemoved

def tokenizeDataFrame(dataFrame):
    tokenizer = Tokenizer(inputCol="_2", outputCol="words")
    tokenizedData = tokenizer.transform(dataFrame)
    return tokenizedData

def removeStopWords(tokenizedData):
    remover = StopWordsRemover(inputCol="words", outputCol="wordsremoved")
    wordsDataRemoved = remover.transform(tokenizedData)
    wordsDataRemoved = wordsDataRemoved.filter("wordsremoved is not NULL")
    return wordsDataRemoved
	
def loadCVModel(path):
    modelCV = CountVectorizerModel.load(path)
    return modelCV
	
def loadTFIDFModel(path):
    modelTfidf = IDFModel.load(path)
    return modelTfidf

def mapDataToLabelsAndFeatures(transformedData):
    data = transformedData.rdd.map(lambda x: LabeledPoint(x["_1"], x["features"].toArray()))
    print data.take(20)
    return data
	
def mapDataToLabelsAndPredictFeatures(model, transformedData):
    predictedData = transformedData.rdd.map(lambda x: (x["_1"], model.predict(x["features"].toArray())))
    print predictedData.take(20)
    return predictedData

def loadWord2VecModel(path):
    word2vec_model = Word2Vec.load(path)
    return word2vec_model

from gensim.models import Word2Vec
from bs4 import BeautifulSoup
import numpy as np
import glob
import pandas as pd

def cleanSent(text):
    text = text.lower()
    text = re.sub(r'http\S+', '', text)
    text = text.replace("via", " ")
    text = text.replace("$", "dollar")
    text = BeautifulSoup(text, "lxml").get_text()
    text = "".join([ch for ch in text if ch not in string.punctuation])
    tokens = text.split(" ")
    cleaned = []
    for item in tokens:
        if not item.isdigit():  # item not in stop
            item = "".join([e for e in item if e.isalnum()])
            if item:
                cleaned.append(item)
    if cleaned:
        return cleaned
    else:
        return [""]

def parseUnlabelledTweetsForWord2Vec(tweetText, word2vec_model, num_features):
    print('===== parseUnlabelledTweetsForWord2Vec =====')
    index2word_set = set(word2vec_model.wv.index2word)
    featureVec = np.zeros((num_features,), dtype="float32")
    nwords = 0.0
    text = tweetText
    text = cleanText(text)
    label = 0.0
    for word in cleanSent(text):
        word = lemmer.lemmatize(word)
        if word and word in index2word_set:  # (name.upper() for name in USERNAMES)
            # if word and word not in stop and word in index2word_set:
            nwords = nwords + 1.0
            featureVec = np.add(featureVec, word2vec_model[word])
    featureVec = np.divide(featureVec, nwords)
    featureVec = np.nan_to_num(featureVec)
    return LabeledPoint(label, featureVec)
	
if __name__ == '__main__':
    sc = SparkContext('local[*]', appName="NaiveBayesModelTrain")
     
    num_features = 300 
    word2vec_model = loadWord2VecModel("C:\\Users\\marin\\Desktop\\SparkTweets\\Models\\lem_model")
    
    print("====== example =======")
    example = word2vec_model.wv["trump"]    
    print (example)
    
   
    
    print("====== most_similar =======")
    print word2vec_model.most_similar(positive=['president', 'america'], negative=['woman'], topn=1)
    
    print("====== doesnt_match =======")
    print word2vec_model.doesnt_match("trump america facebook president".split())
    
    print("====== similarity =======")
    print word2vec_model.wv.similarity('president', 'trump')
  
    list_of_files = []
    list_of_files.extend(glob.glob("C:\\Users\\marin\\Desktop\\bbc\\sport\*.txt"))
    sport = pd.DataFrame()
    for name in list_of_files:
        sport = sport.append(pd.read_csv(name, header=None, quoting=csv.QUOTE_NONE ,delimiter='\t', encoding = "ISO-8859-1").dropna())
    
    print("====== sport =======")
    print(sport)
    
    print("====== accuracy =======")
    word2vec_model.wv.accuracy("C:\\Users\\marin\\Desktop\questions-words.txt")
    '''
	#data
    training = getNaiveBayesModelData(sc, "C:\\Users\marin\\Desktop\\CLASSIFICATION SPARK\\twitter-news-classification-spark-master\\batch_data\\*.csv")
    test = getNaiveBayesModelData(sc, "C:\\Users\marin\\Desktop\\CLASSIFICATION SPARK\\twitter-news-classification-spark-master\\batch_data\\*.csv")
    
    tfidfModelPath = "C:\\Users\\marin\\Desktop\\SparkTweets\\Models\\tfidf_model"
    tfidfModel = loadTFIDFModel(tfidfModelPath)
    
    cvModelPath = "C:\\Users\\marin\\Desktop\\SparkTweets\\Models\\cv_model"
    cvModel = loadCVModel(cvModelPath)
    
    spark = SparkSession(sc)
    
    trainingDataFrame = createDataFrameData(spark, training)
    trainingTokenizedData = tokenizeDataFrame(trainingDataFrame)
    trainingStopWordsRemoved = removeStopWords(trainingTokenizedData)    
    convertedTrainingDataFrame = createAndMapDataToDataFrame(trainingStopWordsRemoved)
	
	#test data
    testDataFrame = createDataFrameData(spark, test)
    testTokenizedData = tokenizeDataFrame(testDataFrame)
    testStopWordsRemoved = removeStopWords(testTokenizedData)    
    convertedTestDataFrame = createAndMapDataToDataFrame(testStopWordsRemoved)
	
	#train
    trainingCVTransformed = cvModel.transform(convertedTrainingDataFrame)
    trainingTFIDFTransformed = tfidfModel.transform(trainingCVTransformed)
	
	#test
    testCVTransformed = cvModel.transform(convertedTestDataFrame)
    testTFIDFTransformed = tfidfModel.transform(testCVTransformed)
	   
    trainingBayes = mapDataToLabelsAndFeatures(trainingTFIDFTransformed)
    
    bayesModelPath = "C:\\Users\\marin\\Desktop\\SparkTweets\\ClassificationModels\\NaiveBayesModel"
    model = NaiveBayes.train(trainingBayes)
    model.save(sc, bayesModelPath)
    
    testBayes = mapDataToLabelsAndPredictFeatures(model, testTFIDFTransformed)
    
    metrics = MulticlassMetrics(testBayes.map(lambda x: (float(x[0]), float(x[1]))))
    
    print metrics.accuracy
    print metrics.confusionMatrix().toArray()
    '''