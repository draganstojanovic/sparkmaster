from nltk.stem.wordnet import WordNetLemmatizer

from gensim import models
import numpy as np
import pandas as pd
import glob
import csv


from nltk.corpus import stopwords
from nltk.tokenize import RegexpTokenizer

lemmer = WordNetLemmatizer()
stop = stopwords.words('english')
alpha_tokenizer = RegexpTokenizer('[A-Za-z]\w+')

def lemmatizeWords(words):
    words = words.lower()
    tokens = alpha_tokenizer.tokenize(words)
    for index, word in enumerate(tokens):
            tokens[index] = lemmer.lemmatize(word)
    return tokens

def loadBBCCorpus():
	list_of_files = []
	list_of_files.extend(glob.glob("C:\\SparkMaster\\ArticlesCorpora\\sport\*.txt"))
	list_of_files.extend(glob.glob("C:\\SparkMaster\\ArticlesCorpora\\business\*.txt"))
	list_of_files.extend(glob.glob("C:\\SparkMaster\\ArticlesCorpora\\entertainment\*.txt"))
	list_of_files.extend(glob.glob("C:\\SparkMaster\\ArticlesCorpora\\politics\*.txt"))
	list_of_files.extend(glob.glob("C:\\SparkMaster\\ArticlesCorpora\\tech\*.txt"))
	bbcCorpus = pd.DataFrame()
	for name in list_of_files:
		bbcCorpus = bbcCorpus.append(pd.read_csv(name, header=None, quoting=csv.QUOTE_NONE ,delimiter='\t', encoding = "ISO-8859-1").dropna())
	return bbcCorpus

def loadKaggleCorpus():
	news1 = pd.read_csv('C:\\SparkMaster\\ArticlesCorpora\\articles\\articles1.csv',encoding = "utf-8")
	news2  = pd.read_csv('C:\\SparkMaster\\ArticlesCorpora\\articles\\articles2.csv',encoding = "utf-8")
	news3  = pd.read_csv('C:\\SparkMaster\\ArticlesCorpora\\articles\\articles3.csv',encoding = "utf-8")
	news1.index = range(0,news1.shape[0])
	news2.index = range(news1.shape[0],news1.shape[0]+news2.shape[0])
	news3.index = range(news1.shape[0]+news2.shape[0],news1.shape[0]+news2.shape[0]+news3.shape[0])
	news = pd.concat([news1, news2, news3])
	custom_news = news['content']
	kaggleNewsCorpus = pd.DataFrame(custom_news)
	kaggleNewsCorpus.columns = ['0']
	return kaggleNewsCorpus

def concatenateCorpora(bbcCorpus, kaggleCorpus):
	corpus = np.concatenate([bbcCorpus.values, kaggleCorpus.values])
	return corpus

def trainWord2VecModel(lemmatizedCorpus, path):
	import logging
	logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

	#train word2vec model
	min_count = 10
	size = 300
	window = 10
	model_lemmatized = models.Word2Vec(corpus_lemmatized, min_count=min_count, size=size, window=window)
	model_lemmatized.save(path)
	
#load corpora	
bbcCorpus = loadBBCCorpus()
kaggleCorpus = loadKaggleCorpus()
#concatenate corpora
corpus = concatenateCorpora(bbcCorpus, kaggleCorpus)
#lemmatize corpora
corpus_lemmatized = [lemmatizeWords(textCorpus[0]) for textCorpus in corpus]
#train word2vec
trainWord2VecModel(corpus_lemmatized, "C:\\SparkMaster\\FeatureModels\\lem_model")
