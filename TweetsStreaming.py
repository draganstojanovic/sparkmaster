
"""
Created on Fri Feb 16 00:23:43 2018

@author: marin
"""

import json
import tweepy
import ConfigParser as configparser

from kafka import KafkaProducer

accounts = {
		"13850422" : 0, #"CNNPolitics"
		"18767699" : 0, #"CBSPolitics"   
       "25562002" : 0, #"ReutersPolitics"
		"16032925" : 0, #"foxnewspolitics"
       "14434063" : 0, #"nytpolitics"
       
		"11014272" : 1, #"ftfinancenews"   
       "19546277" : 1, #"YahooFinance"
		"34713362" : 1, #"business"
       "19643448" : 1, #"FinancialNews"
		"1044606986946531328" : 1, #"CNNMoney"
        
       "7302282" : 2, #"YahooSports"
		"34992644" : 2, #"cnnsport"
       "265902729" : 2, #"BBCSport"
		"46403451" : 2, #"guardian_sport"   
       "30690661" : 2, #"sportingnews"
       
		"42703075" : 3, #"cnntech"
       "19402238" : 3, #"ScienceNews"
		"14885549" : 3, #"ForbesTech"   
       "621583" : 3, #"BBCTech"
		"25562270" : 3, #"ReutersTech"  
        
       "110458336" : 4, #"CNNent"
       "20275150" : 4, #"YahooCelebrity"
		"2883841" : 4, #"enews"   
       "111556701" : 4, #"DailyMailCeleb"
		"16312576" : 4, #"EW"  
        
       "317393966" : 5, #"HuffPostCrime"
       "334739729" : 5, #"CrimeInTheD"
		"66967746" : 5, #"metroliceuk"   
       "48504374" : 5, #"MadisonCrime"
		"18561480" : 5, #"denvercrime" 
	}

def initialize():
    print 'init'
    config = configparser.RawConfigParser()
    config.read('C:\\SparkMaster\\Configuration\\config.properties')
    print 'parsed config'
    
    consumer_token = config.get('Twitter', 'consumer_token')
    consumer_secret = config.get('Twitter', 'consumer_secret')
    access_token = config.get('Twitter', 'access_token')
    access_secret = config.get('Twitter', 'access_secret')
    
    auth = tweepy.OAuthHandler(consumer_token, consumer_secret)
    auth.set_access_token(access_token, access_secret)
    api = tweepy.API(auth)
    print 'got api'
	
    stream = TwitterStreamListener()
    print 'created stream listener'
    twitter_stream = tweepy.Stream(auth = api.auth, listener=stream)
    print 'created stream'
    accountIDS = accounts.keys()
    twitter_stream.filter(follow=accountIDS, async=True)
    #twitter_stream.filter(follow=['778671272247042049', '742143', '428333', '759251', '5402612', '807095','3108351','1652541','51241574','2467791'], async=True)
	
class TwitterStreamListener(tweepy.StreamListener):
    def __init__(self):
      self.producer = KafkaProducer(bootstrap_servers='localhost:9092', value_serializer=lambda v: json.dumps(v))
      print 'created kafka producer'
      self.tweets = []

    def on_data(self, data):
      print 'got data'    
      jsonParsed = json.loads(data)
      text = jsonParsed[u'text']
      idi = jsonParsed[u'id']
      created_at = jsonParsed[u'created_at'] 
      
      expectedLabel = -1
      username = ""
      if u'retweeted_status' in jsonParsed:
          retweeted_status = jsonParsed[u'retweeted_status']      
          print "retweeted_status"
          user = retweeted_status[u'user']
          
      else:
          user = jsonParsed[u'user']
          
      userid = user[u'id_str']
      
      if userid in accounts:
          expectedLabel = accounts[userid]
          username = user[u'screen_name']
          data = {'text':text, 'id':idi, 'created_at':created_at, "expectedLabel":expectedLabel, "account":username}
          dataJson = json.dumps(data)
          print dataJson   
          self.producer.send('twitterTopic', data)
          self.producer.flush()
      
      
      

    def on_error(self, status_code):
        if status_code == 420:
            return False


if __name__ == "__main__":
    initialize()