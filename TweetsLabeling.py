import tweepy
import csv
import re

# Consumer keys and access tokens, used for OAuth
consumer_key = "zZVm4RlrBGzOrd54wlHzeP5kj"
consumer_secret = "02jtCnUIvpM0B5F5VYmNMUmcx8uyFPPxhN3b2qpHzt1vMZY8cx"
access_key = "567510431-K4Oud4FvqL6oq2FGf2WAR9CXn93xR7AUq4j3vQsQ"
access_secret = "LXo2T0N1i07bwcILWpTJPKdmyK8j7tNPIXCyNJ1LCAWCS"

def initializeTwitterAPIConnection(consumerKey, consumerSecret, accessKey, accessSecret):
	# OAuth process, using the keys and tokens
	auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
	auth.set_access_token(access_key, access_secret)

	# Creation of the actual interface, using authentication
	api = tweepy.API(auth)
	return api
	
def retrieveTweetsFromAnAccount(twitterApi, accountName):
    full_text_tweets = []
    print ("=== Fetching tweets from " + accountName + " ===")
    for status in tweepy.Cursor(twitterApi.user_timeline, screen_name=accountName, tweet_mode='extended').items():
        full_text_tweets.append(status._json['full_text'])
	return full_text_tweets
	
def cleanTweets(tweets):
    print ("=== Cleaning tweets... ===")
    cleaned_text = [re.sub(r'http[s]?:\/\/.*[\W]*', '', tweet, flags=re.MULTILINE) for tweet in tweets] # remove urls
    cleaned_text = [re.sub(r'@[\w]*', '', i, flags=re.MULTILINE) for i in cleaned_text] # remove the @twitter mentions 
    cleaned_text = [re.sub(r'RT.*','', i, flags=re.MULTILINE) for i in cleaned_text] # delete the retweets
    cleaned_text = [i.replace('\n', ' ') for i in cleaned_text] #replace enters
    cleaned_text = filter(None, cleaned_text) #remove empty tweets
    return cleaned_text

def labelAndSaveTweets(cleanedTweets, label, folderPath):
    print ("=== Mapping tweets to text label pair... ===")
    outtweets = [[tweet.encode('ascii', 'replace'), label] for tweet in cleanedTweets]
    print ("=== Writing into csv... ===")
    with open(folderPath, 'wb') as f:
        writer = csv.writer(f, delimiter='`', quotechar='|', quoting=csv.QUOTE_ALL)
        writer.writerows(outtweets)

def getTwitterNewsAccountsAndLabels():
	labels = [0, 1, 2, 3, 4, 5]
	accounts = [['@CNNPolitics', '@CBSPolitics', '@ReutersPolitics', '@foxnewspolitics', '@nytpolitics'],
		   ['@ftfinancenews', '@YahooFinance', '@business','@FinancialNews', '@CNNMoney'],
		   ['@YahooSports', '@cnnsport', '@BBCSport','@guardian_sport', '@sportingnews'],
		   ['@cnntech', '@ScienceNews', '@ForbesTech','@BBCTech', '@ReutersTech'],
		   ['@CNNent', '@YahooCelebrity', '@enews','@DailyMailCeleb', '@EW'],
		   ['@HuffPostCrime', '@CrimeInTheD', '@metpoliceuk','@MadisonCrime', '@denvercrime']
		  ]
	dict(zip(labels, zip(*accounts)))
	return dict;
		
if __name__ == '__main__':
	twitterApi = initializeTwitterAPIConnection(consumer_key, consumer_secret, access_key, access_secret)
	labelsAndAccounstDict = getTwitterNewsAccountsAndLabels()
	for label, accounts in labelsAndAccounstDict.iteritems():
		for account in accounts:
			tweets = retrieveTweetsFromAnAccount(account)
			cleanedTweets = cleanTweets(tweets)
			labelAndSaveTweets(cleanedTweets, label, 'C:\\SparkMaster\\LabeledData\\%s_tweets.csv' % account)		
